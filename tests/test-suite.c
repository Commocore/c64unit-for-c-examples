
// Include c64unit test framework
#include <c64unit.h>

// Source code to be unit tested
#include "./../src/sum.c"
#include "./../src/guess-band-by-song-name.c"
#include "./../src/simple-controller.c"

// Test cases
#include "./test-cases/test-sum.c"
#include "./test-cases/test-strings.c"
#include "./test-cases/test-mocking.c"

// Run test suite
int main(void) {
	
	// Init
	c64unit(true);
	
	// Examine test cases
	testSum();
	testGreater();
	testGreaterOrEqual();
	testStringsEqual();
	testStringsNotEqual();
	testMocking();
	testUnmocking();
	
	// If this point is reached, there were no assertion fails
	return c64unit_exit();
}
