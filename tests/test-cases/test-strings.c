
void testStringsEqual() {
	char *songs[] = {"First Days Of Winter", "The Last Song"};
	char *bands[] = {"Asylum Party", "Trisomie 21"};

	unsigned i;
	for (i = 0; i < 2; ++i) {
		c64unit_assertCharEqual(
			bands[i],
			guessBandBySongName(songs[i]),
			"No clue what's the band for this song!"
		);
	}
}

void testStringsNotEqual() {
	c64unit_assertCharNotEqual(
		"Asylum Party",
		guessBandBySongName("The Last Song"),
		"I think this is not the right answer!"
	);
}
