
void testSum() {
	c64unit_assertIntEqual(11, sum(5, 6), "Sum test failed.");
	c64unit_assertIntNotEqual(0, sum(5, 6), "");
}

void testGreater() {
	c64unit_assertGreater(50, sum(30, 19), "I say that 50 isn't greater than actual");
}

void testGreaterOrEqual() {
	c64unit_assertGreaterOrEqual(50, sum(30, 20), "I say that 50 isn't >= than actual");
}
