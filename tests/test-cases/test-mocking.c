
bool isAccessibleMock() {
	return true;
}

void testMocking() {
	// Mock isAccessible() method
	#define isAccessible isAccessibleMock
	
	// Init controller
	initSimpleController();
	
	// Assertions
	c64unit_assertTrue(isAccessible(), "Should be accessible...");
	c64unit_assertIntEqual(100, getCoordinate(), "We're lost!");
}

void testUnmocking() {
	// Unmock isAccessible() method
	#undef isAccessible
	
	// Init controller
	initSimpleController();
	
	// Assertion
	c64unit_assertFalse(isAccessible(), "Shouldn't be accessible...");
}
