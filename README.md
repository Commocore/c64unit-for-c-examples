
c64unit for C examples
======================

This is a "playground" for *[c64unit for C](https://bitbucket.org/Commocore/c64unit-for-c)*, the ultimate unit test
framework for Commodore 64, which supports testing in C language within [cc65](https://github.com/cc65/cc65).


![c64unit](http://www.commocore.com/images/external/c64unit/c64unit-logo.png)


# 1. About.

This repository contains examples with a variety of assertions including value assertions and assertions of Data Sets.
You can learn also how to mock methods, and in general, how to create your test suites to test your code.

Note, that this repository uses only the newest version of *c64unit for C* framework, and all examples will be updated if necessary.
For a reference with former versions, you can always check the history of commits.

Check how easily - with just a few lines of test code - you can focus on the implementation, and improve your code as you go!


# 2. How to start.

Full documentation is available on the *c64unit for C* repository page: [https://bitbucket.org/Commocore/c64unit-for-c](https://bitbucket.org/Commocore/c64unit-for-c).


# 3. Support.

If you'll find any bug, or test case which fails for any reason, please open the issue.
