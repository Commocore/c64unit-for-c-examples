
#include <string.h>

char * guessBandBySongName(char * songName) {
	if (!strcmp(songName, "First Days Of Winter")) {
		return "Asylum Party";
	}
	if (!strcmp(songName, "The Last Song")) {
		return "Trisomie 21";
	}
	return "";
}
