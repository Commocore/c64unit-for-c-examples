
#include <stdbool.h>

static bool access;
static int coordinate;

void initSimpleController()
{
	access = false;
	coordinate = 100;
}

bool isAccessible() {
	return access;
}

int getCoordinate() {
	return coordinate;
}

int simpleController() {
	if (isAccessible()) {
		getCoordinate();
	} else {
		return 0;
	}
}
